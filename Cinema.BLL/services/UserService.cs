﻿using Cinema.DAL.Repositories;
using Cinema.DAL.Domain;
using Cinema.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cinema.Shared.DTO;
using Cinema.DAL;
using Cinema.DAL.Domain.Exceptions;
using Cinema.BLL.services;
using Cinema.DAL.Helpers;

namespace Cinema.BLL
{
    public interface IUserService
    {
        Task<UserDTO> GetById(Guid id);
        Task<UserDTO> GetByEmailAsync(string email);
        Task<IEnumerable<UserDTO>> GetAllAsync();
        Task AddUserAsync(Guid userId, string email, string password, string username, string role);
        Task UpdateAsync(User user);
        Task RemoveAsync(Guid id);
        Task <string> Authenticate(AuthenticateRequest model);
    }
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IJwtService _jwtService;
        public UserService(IUserRepository userRepository, IJwtService jwtService)
        {
            _userRepository = userRepository;
            _jwtService = jwtService;
        }
        public async Task<UserDTO> GetById(Guid id)
        {
            if(!await _userRepository.DoesExist(id))
            {
                throw new CinemaException(ErrorCodes.NotExist, $"User with id: {id} does not exist.");
            }
            var user = await _userRepository.GetAsync(id);
            return user.MapUser();
        }
        public async Task<UserDTO> GetByEmailAsync(string email)
        {
            if (!await _userRepository.DoesExist(email))
            {
                throw new CinemaException(ErrorCodes.NotExist, $"User with email: {email} does not exist.");
            }
            var user = await _userRepository.GetByEmailAsync(email);
            return user.MapUser();
        }
        public async Task<IEnumerable<UserDTO>> GetAllAsync()
        {
            var users = await _userRepository.GetAllAsync();
            return users.Select(x => x.MapUser());
        }
        public async Task AddUserAsync(Guid userId, string email, string password, string username, string role)
        {
            string passwordHash = BCrypt.Net.BCrypt.HashPassword(password);
            var user = new User(userId, email, passwordHash, username, role);
            await _userRepository.AddAsync(user);
        }
        public async Task <string> Authenticate(AuthenticateRequest model)
        {
            if (!await _userRepository.DoesExist(model.Email))
            {
                throw new CinemaException(ErrorCodes.NotExist, $"User with email: {model.Email} does not exist.");
            }
            var user = await _userRepository.GetByEmailAsync(model.Email);
            if (!BCrypt.Net.BCrypt.Verify(model.Password, user.Password))
            {
                throw new CinemaException(ErrorCodes.InvalidCredentials, "Invalid credentials.");
            }
            return _jwtService.generateJwtToken(user);
        }
        public async Task RemoveAsync(Guid id)
        {
            await _userRepository.RemoveAsync(id);
        }
        public async Task UpdateAsync(User user)
        {
            await _userRepository.UpdateAsync(user);
        }
    }
}