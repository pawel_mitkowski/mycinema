﻿using Cinema.DAL.Helpers;
using Cinema.DAL.Domain;
using Cinema.DAL.Domain.Exceptions;
using Cinema.DAL.Repositories;
using Cinema.Shared.DTO;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Cinema.BLL.services
{
    public interface IOrderService
    {
        Task<OrderDTO> GetbyId(Guid id);
        Task<IEnumerable<OrderDTO>> GetAllAsync();
        Task SubmitOrder(Guid myPerformanceId, List<BookedSeatDTO> bookedSeats);
        Task RemoveOrder(Guid id);
        Task UpdateAsync(Order order);
    }
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IHallRepository _hallRepository;
        private readonly IPerformanceRepository _performanceRepository;
        public OrderService(IOrderRepository orderRepository, IHallRepository hallRepository, IPerformanceRepository performanceRepository)
        {
            _orderRepository = orderRepository;
            _hallRepository = hallRepository;
            _performanceRepository = performanceRepository;
        }
        public async Task<OrderDTO> GetbyId(Guid id)
        {
            var order = await _orderRepository.GetAsync(id);
            if (order == null)
            {
                throw new CinemaException(ErrorCodes.NotExist, $"Order with id: '{id}' does not exist.");
            }
            return order.MapOrder();
        }
        public async Task<IEnumerable<OrderDTO>> GetAllAsync()
        {
            var orders = await _orderRepository.GetAllAsync();
            return orders.Select(x => x.MapOrder());
        }
        public async Task RemoveOrder(Guid id)
        {
            await _orderRepository.RemoveAsync(id);
        }
        public async Task SubmitOrder(Guid myPerformanceId, List<BookedSeatDTO> bookedSeats)
        {
            var performance = await _performanceRepository.GetAsync(myPerformanceId);
            if (performance == null)
            {
                throw new CinemaException(ErrorCodes.NotExist, $"Performance with id: {myPerformanceId} does not exist.");
            }

            var hall = await _hallRepository.GetAsync(performance.PerformanceHallNumber);
            foreach (var bookedSeat in bookedSeats)
            {
                if (bookedSeat.SeatNumber > hall.SeatsInRow || bookedSeat.RowNumber > hall.Rows)
                {
                    throw new CinemaException(ErrorCodes.InvalidSeatData, "Invalid seat data.");
                }
            }

            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                foreach (var bookedSeat in bookedSeats)
                {
                    if (await _orderRepository.IsSeatReserved(myPerformanceId, bookedSeat.RowNumber, bookedSeat.SeatNumber))
                    {
                        throw new CinemaException(ErrorCodes.AlreadyReserved, "Seat already reserved");
                    }
                    var order = new Order(Guid.NewGuid(), myPerformanceId, bookedSeat.SeatNumber, bookedSeat.RowNumber);
                    await _orderRepository.AddAsync(order);
                }
                scope.Complete();
            }
        }
        public async Task UpdateAsync(Order order)
        {
            await _orderRepository.UpdateAsync(order);
        }
    }
}
