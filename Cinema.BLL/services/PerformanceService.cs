﻿using Cinema.DAL.Helpers;
using Cinema.DAL.Domain;
using Cinema.DAL.Domain.Exceptions;
using Cinema.DAL.Repositories;
using Cinema.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema.BLL.services
{
    public interface IPerformanceService
    {
        Task<PerformanceDTO> GetById(Guid id);
        Task<IEnumerable<PerformanceDTO>> GetAllAsync();
        Task<IEnumerable<HallSeatsDTO>> CreateHallVisualization(Guid performanceId);
        Task AddPerformanceAsync(Guid movieId, string performanceHallNumber, DateTime movieTime);
        Task UpdateAsync(Performance performance);
        Task Remove(Guid id);
    }
    public class PerformanceService : IPerformanceService
    {
        private readonly IPerformanceRepository _performanceRepository;
        private readonly IMovieRepository _movieRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IHallRepository _hallRepository;
        public PerformanceService(IPerformanceRepository performanceRepository, IMovieRepository movieRepository, IOrderRepository orderRepository, IHallRepository hallRepository)
        {
            _performanceRepository = performanceRepository;
            _movieRepository = movieRepository;
            _orderRepository = orderRepository;
            _hallRepository = hallRepository;
        }
        public async Task<PerformanceDTO> GetById(Guid id)
        {
            var performance = await _performanceRepository.GetAsync(id);
            if (performance == null)
            {
                throw new CinemaException(ErrorCodes.NotExist, "Performance does not exist.");
            }
            return performance.MapPerformance();
        }
        public async Task<IEnumerable<PerformanceDTO>> GetAllAsync()
        {
            var performances = await _performanceRepository.GetAllAsync();
            return performances.Select(x => x.MapPerformance());
        }
        public async Task AddPerformanceAsync(Guid movieId, string performanceHallNumber, DateTime movieTime)
        {
            var movie = await _movieRepository.GetAsync(movieId);
            if (movie == null)
            {
                throw new CinemaException(ErrorCodes.NotExist, $"Movie with ID {movieId} does not exist.");
            }
            if (!await _hallRepository.DoesExist(performanceHallNumber))
            {
                throw new CinemaException(ErrorCodes.NotExist, $"Hall with number{performanceHallNumber} does not exist.");
            }
            var performance = new Performance(Guid.NewGuid(), movie.Name, performanceHallNumber, movieTime);
            await _performanceRepository.AddAsync(performance);
        }
        public async Task<IEnumerable<HallSeatsDTO>> CreateHallVisualization(Guid performanceId)
        {
            var performance = await _performanceRepository.GetAsync(performanceId);
            if (performance == null)
            {
                throw new CinemaException(ErrorCodes.NotExist, "Performance does not exist.");
            }
            var hall = await _hallRepository.GetAsync(performance.PerformanceHallNumber);
            var seatsList = new List<HallSeatsDTO>();
            for (int i = 1; i <= hall.Rows; i++)
            {
                for (int x = 1; x <= hall.SeatsInRow; x++)
                {
                    bool isSeatReserved = await _orderRepository.IsSeatReserved(performanceId, i, x);

                    var hallSeat = new HallSeatsDTO(i, x, isSeatReserved);
                    seatsList.Add(hallSeat);
                }
            }
            return seatsList;
        }
        public async Task UpdateAsync(Performance performance)
        {
            await _performanceRepository.UpdateAsync(performance);
        }
        public async Task Remove(Guid id)
        {
            await _performanceRepository.RemoveAsync(id);
        }
    }
}