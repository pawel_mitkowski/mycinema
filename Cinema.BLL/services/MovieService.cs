﻿using Cinema.DAL.Helpers;
using Cinema.DAL.Domain;
using Cinema.Shared.DTO;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Cinema.DAL.Repositories;
using System.Collections.Generic;
using Cinema.DAL.Domain.Exceptions;

namespace Cinema.BLL
{
    public interface IMovieService
    {
        Task<MovieDTO> GetById(Guid id);
        Task<IEnumerable<MovieDTO>> GetAllAsync();
        Task AddMovieAsync(string name, int minutes);
        Task UpdateAsync(Movie movie);
        Task Remove(Guid id);
    }
    public class MovieService : IMovieService
    {
        private readonly IMovieRepository _movieRepository;
        public MovieService(IMovieRepository movieRepository)
        {
            _movieRepository = movieRepository;
        }
        public async Task<MovieDTO> GetById(Guid id)
        {
            var movie = await _movieRepository.GetAsync(id);
            if (movie == null)
            {
                throw new CinemaException(ErrorCodes.NotExist, $"Movie with id: '{id}' does not exist.");
            }
            return movie.MapMovie();
        }
        public async Task<IEnumerable<MovieDTO>> GetAllAsync()
        {
            var movies = await _movieRepository.GetAllAsync();
            if (movies == null)
            {
                throw new CinemaException(ErrorCodes.NotExist, $"Movies list is empty.");
            }
            return movies.Select(x => x.MapMovie());
        }
        public async Task AddMovieAsync(string name, int minutes)
        {
            if (await _movieRepository.DoesExist(name))
            {
                throw new CinemaException(ErrorCodes.AlreadyExist, $"Movie with name: '{name}' already exists.");
            }
            var movie = new Movie(Guid.NewGuid(), name, minutes);
            await _movieRepository.AddAsync(movie);
        }
        public async Task UpdateAsync(Movie movie)
        {
            await _movieRepository.UpdateAsync(movie);
        }
        public async Task Remove(Guid id)
        {
            await _movieRepository.RemoveAsync(id);
        }
    }
}