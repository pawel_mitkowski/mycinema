﻿using System;

namespace Cinema.Shared.DTO
{
    public class MovieDTO
    {
        public string Name { get; set; }
        public string Length { get; set; }
    }
}