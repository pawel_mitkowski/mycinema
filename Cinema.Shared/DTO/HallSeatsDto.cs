﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cinema.Shared.DTO
{
    public class HallSeatsDTO
    {
        public int Row { get; set; }
        public int SeatNumber { get; set; }
        public bool IsReserved { get; set; }
        public HallSeatsDTO(int row, int seatNumber, bool isReserved)
        {
            Row = row;
            SeatNumber = seatNumber;
            IsReserved = isReserved;
        }
    }
}
