﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cinema.Shared.DTO
{
    public class BookedSeatDTO
    {
        public int RowNumber { get; set; }
        public int SeatNumber { get; set; }
    }
}
