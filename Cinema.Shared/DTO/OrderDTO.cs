﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cinema.Shared.DTO
{
    public class OrderDTO
    {
        public Guid OrderId { get; set; }
        public Guid MyPerformanceId { get; set; }
        public int SeatNumber { get; set; }
        public int Row { get; set; }
        public bool IsSeatReserved { get; private set; }
    }
}
