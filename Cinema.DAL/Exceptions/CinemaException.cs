using System;
using System.Globalization;

namespace Cinema.DAL.Domain.Exceptions
{
    public class CinemaException : Exception
    {
        public string Code { get; }
        protected CinemaException()
        {
        }
        protected CinemaException(string code)
        {
            Code = code;
        }
        public CinemaException(string code, string message) : base(message)
        {
            Code = code;
        }
        public CinemaException(string code, string message, params object[] args) : base(message)
        {
            Code = code;
        }
    }
}