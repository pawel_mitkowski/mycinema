﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cinema.DAL.Domain.Exceptions
{
    public class ErrorCodes
    {
        public static string InvalidUsername => "invalid_username";
        public static string InvalidHallNumber => "invalid_hall_number";
        public static string AlreadyExist => "object_already_exists";
        public static string NotExist => "object_does_not_exist";
        public static string AlreadyReserved => "seat_already_reserved";
        public static string InvalidCredentials => "invalid_credentials";
        public static string InvalidRole => "invalid_role";
        public static string InvalidPassword => "invalid_password";
        public static string InvalidEmail => "invalid_email";
        public static string InvalidSeatData => "invalid_seat_data";
    }
}
