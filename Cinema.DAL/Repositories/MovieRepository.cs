﻿using Cinema.DAL.Context;
using Cinema.DAL.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cinema.DAL.Repositories
{
    public interface IMovieRepository
    {
        Task<Movie> GetAsync(Guid id);
        Task<Movie> GetAsync(string name);
        Task<IEnumerable<Movie>> GetAllAsync();
        Task AddAsync(Movie movie);
        Task UpdateAsync(Movie movie);
        Task RemoveAsync(Guid id);
        Task<bool> DoesExist(string name);
    }
    public class MovieRepository : IMovieRepository
    {
        private readonly CinemaDbContext _dbContext;
        public MovieRepository(CinemaDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<Movie> GetAsync(Guid id)
            => await _dbContext.Movies.SingleOrDefaultAsync(x => x.Id == id);
        public async Task<Movie> GetAsync(string name)
            => await _dbContext.Movies.SingleOrDefaultAsync(x => x.Name == name);
        public async Task<IEnumerable<Movie>> GetAllAsync()
            => await _dbContext.Movies.ToListAsync();
        public async Task AddAsync(Movie movie)
        {
            await _dbContext.Movies.AddAsync(movie);
            await _dbContext.SaveChangesAsync();
        }
        public async Task RemoveAsync(Guid id)
        {
            var movie = await GetAsync(id);
            _dbContext.Movies.Remove(movie);
            await _dbContext.SaveChangesAsync();
        }
        public async Task UpdateAsync(Movie movie)
        {
            _dbContext.Movies.Update(movie);
            await _dbContext.SaveChangesAsync();
        }
        public async Task<bool> DoesExist(string name)
        {
            return await _dbContext.Movies.AnyAsync(x => x.Name == name);
        }
    }
}