﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Cinema.DAL.Domain
{
    public class Hall
    {
        public Hall()
        {
        }
        [Key]
        public string HallNumber { get; private set; }
        public int SeatsInRow { get; private set; }
        public int Rows { get; private set; }
        public int NumberOfSeats { get; private set; }
        public Hall(string hallNumber, int seatsInRow, int numberOfRows)
        {
            SetHallNumber(hallNumber);
            SetSeatsInRow(seatsInRow);
            SetRows(numberOfRows);
            NumberOfSeats = seatsInRow * numberOfRows;
        }
        public void SetHallNumber(string hallNumber)
        {
            HallNumber = hallNumber;
        }
        public void SetSeatsInRow(int seatsInRow)
        {
            SeatsInRow = seatsInRow;
        }
        public void SetRows(int rowsNumber)
        {
            Rows = rowsNumber;
        }
    }
}