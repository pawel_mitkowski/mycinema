﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cinema.DAL.Domain
{
    public class Movie
    {
        public Movie() {}
        public Guid Id { get; set; }
        public string Name { get; set;  }
        public int Length { get; set; }
        public Movie(Guid id, string name, int length)
        {
            Id = id;
            Name = name;
            Length = length;
        }
    }
}
