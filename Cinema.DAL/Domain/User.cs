﻿using Cinema.DAL.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cinema.DAL.Domain
{
    public class User
    {
        public User()
        {
        }
        public Guid Id { get; protected set; }
        public string Email { get; protected set; }
        public string Password { get; protected set; }
        public string Username { get; protected set; }
        public string Role { get; protected set; }
        public DateTime CreatedAt { get; protected set; }
        public DateTime UpdatedAt { get; protected set; }
        public User(Guid userId, string email, string password, string username, string role)
        {
            Id = userId;
            SetEmail(email);
            SetPassword(password);
            CreatedAt = DateTime.UtcNow;
            SetUsername(username);
            SetRole(role);
            CreatedAt = DateTime.UtcNow;
        }
        public void SetUsername(string username)
        {
            if (String.IsNullOrEmpty(username))
            {
                throw new CinemaException(ErrorCodes.InvalidUsername, "Username is empty.");
            }
            Username = username.ToLowerInvariant().Trim();
            UpdatedAt = DateTime.UtcNow;
        }
        public void SetEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                throw new CinemaException(ErrorCodes.InvalidEmail, "Email cannot be empty.");
            }
            if (Email == email)
            {
                return;
            }
            Email = email.ToLowerInvariant().Trim();
            UpdatedAt = DateTime.UtcNow;
        }
        public void SetRole(string role)
        {
            if (string.IsNullOrWhiteSpace(role))
            {
                throw new CinemaException(ErrorCodes.InvalidRole, "Role cannot be empty.");
            }
            if (Role == role)
            {
                return;
            }
            Role = role.ToLowerInvariant().Trim();
            UpdatedAt = DateTime.UtcNow;
        }
        public void SetPassword(string password)
        {
            if (string.IsNullOrWhiteSpace(password))
            {
                throw new CinemaException(ErrorCodes.InvalidPassword, "Password can not be empty.");
            }
            if (password.Length < 4)
            {
                throw new CinemaException(ErrorCodes.InvalidPassword, "Password must contain at least 4 characters.");
            }
            if (password.Length > 100)
            {
                throw new CinemaException(ErrorCodes.InvalidPassword, "Password can not contain more than 50 characters.");
            }
            if (Password == password)
            {
                return;
            }
            Password = password.Trim();
            UpdatedAt = DateTime.UtcNow;
        }
    }
}