﻿CREATE TABLE [dbo].[Performances]
(
	[PerformanceId] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [MovieName] NVARCHAR(100) NOT NULL, 
    [PerformanceHallNumber] NCHAR(10) NOT NULL, 
    [MovieTime] DATETIME NOT NULL, 
    [TicketPrice] DECIMAL NULL, 
)
