﻿CREATE TABLE [dbo].[Halls]
(
	[Hallnumber] NVARCHAR(50) NOT NULL PRIMARY KEY, 
    [SeatsInRow] INT NOT NULL, 
    [Rows] INT NOT NULL, 
    [NumberOfSeats] INT NOT NULL
)
