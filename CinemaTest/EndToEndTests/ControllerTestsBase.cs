﻿using Cinema.BLL;
using Cinema.DAL.Domain;
using Cinema.Shared.DTO;
using Cinema.WebApi;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace CinemaTests.EndToEndTests
{
    public class UserControllerTests : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> _factory;

        public UserControllerTests(WebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task given_invalid_email_user_shoul_not_exist()
        {
            var client = _factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureTestServices(services =>
                {
                    services.AddScoped<IUserService, UserServiceMock>();
                });
            }).CreateClient();

            var response = await client.GetAsync($"user/details/list");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
    }

    public class UserServiceMock : IUserService
    {
        public Task AddUserAsync(Guid userId, string email, string password, string username, string role)
        {
            throw new NotImplementedException();
        }

        public Task<string> Authenticate(AuthenticateRequest model)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<UserDTO>> GetAllAsync()
        {
            return new List<UserDTO>();
        }

        public Task<UserDTO> GetByEmailAsync(string email)
        {
            throw new NotImplementedException();
        }

        public Task<UserDTO> GetById(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task RemoveAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(User user)
        {
            throw new NotImplementedException();
        }
    }
}
