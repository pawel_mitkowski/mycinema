using Cinema.BLL;
using Cinema.BLL.services;
using Cinema.DAL;
using Cinema.DAL.Domain;
using Cinema.DAL.Domain.Exceptions;
using Cinema.DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace CinemaTest
{
    public class UserServiceTests
    {
        [Fact]
        public async Task Add_user_async_should_invoke_add_async_on_repository()
        {
            var userRepositoryMock = new Mock<IUserRepository>();
            var jwtServiceMock = new Mock<IJwtService>();
            var userService = new UserService(userRepositoryMock.Object, jwtServiceMock.Object);
            await userService.AddUserAsync(Guid.NewGuid(), "email@google.com", "pass", "Gazza", "admin");
            userRepositoryMock.Verify(x => x.AddAsync(It.IsAny<User>()), Times.Once);
        }
        [Fact]
        public async Task User_service_should_get_expected_data_when_invoke_GetbyID_method()
        {
            var expectedId = Guid.NewGuid();
            var expectedEmail = "email";
            var expectedUsername = "username";
            var expectedRole = "admin";
            var userRepositoryMock = new Mock<IUserRepository>();
            var jwtServiceMock = new Mock<IJwtService>();
            userRepositoryMock.Setup(x => x.GetAsync(It.IsAny<Guid>())).ReturnsAsync(new User(expectedId, expectedEmail, "password", expectedUsername, expectedRole));

            var userService = new UserService(userRepositoryMock.Object, jwtServiceMock.Object);
            var result = await userService.GetById(It.IsAny<Guid>());

            Assert.Equal(expectedId, result.Id);
            Assert.Equal(expectedEmail, result.Email);
            Assert.Equal(expectedUsername, result.Username);
            Assert.Equal(expectedRole, result.Role);
        }
        [Fact]
        public async Task User_service_should_throw_exception_when_user_does_not_exist()
        {
            var userRepositoryMock = new Mock<IUserRepository>();
            var jwtServiceMock = new Mock<IJwtService>();
            var userService = new UserService(userRepositoryMock.Object, jwtServiceMock.Object);
            userRepositoryMock.Setup(x => x.GetAsync(It.IsAny<Guid>())).ReturnsAsync((User)null);
            var exception = await Assert.ThrowsAsync<CinemaException>(() => userService.Authenticate(new AuthenticateRequest {Email="notxist", Password="secret" }));
            Assert.Equal(ErrorCodes.NotExist, exception.Code);
        }
        [Fact]
        public async Task User_service_should_throw_exception_when_password_does_not_match()
        {
            var user = new User(Guid.NewGuid(), "email1", BCrypt.Net.BCrypt.HashPassword("password1"), "username", "admin");
            var authenticatedRequest = new AuthenticateRequest
            {
                Email = "notexist",
                Password = "secret"
            };
            var userRepositoryMock = new Mock<IUserRepository>();
            var jwtServiceMock = new Mock<IJwtService>();
            var userService = new UserService(userRepositoryMock.Object, jwtServiceMock.Object);
            userRepositoryMock.Setup(x => x.GetByEmailAsync(It.IsAny<string>())).ReturnsAsync(user);

            var exception = await Assert.ThrowsAsync<CinemaException>(() => userService.Authenticate(authenticatedRequest));

            Assert.Equal(ErrorCodes.InvalidCredentials, exception.Code);
        }
    }
}
