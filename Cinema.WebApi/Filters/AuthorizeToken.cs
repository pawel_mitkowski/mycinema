﻿using Cinema.BLL.services;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cinema.WebApi.Filters
{
    public class AuthorizeToken : IAsyncActionFilter
    {
        private readonly IJwtService _jwtService;

        public AuthorizeToken(IJwtService jwtService)
        {
            _jwtService = jwtService;
        }

        public async Task OnActionExecutionAsync(
            ActionExecutingContext context,
            ActionExecutionDelegate next)
        {
            var token = context.HttpContext.Request.Headers["Authorization"];
            var response = _jwtService.ValidateJwtToken(token);
            if(response==null)
            {
                throw new Exception("brak dostepu");
            }
            context.HttpContext.Items["User"] = response;

            // Do something before the action executes.

            // next() calls the action method.
            var resultContext = await next();
            // resultContext.Result is set.
            // Do something after the action executes.
        }
    }
}
