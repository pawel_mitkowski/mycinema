﻿using Cinema.BLL;
using Cinema.BLL.services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Cinema.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HallController : ControllerBase
    {
        private readonly ILogger<HallController> _logger;
        private readonly IHallService _hallService;
        public HallController(IHallService hallService, ILogger<HallController> logger)
        {
            _hallService = hallService;
            _logger = logger;
        }
        [HttpGet]
        [Route("details/{hallNumber}")]
        public async Task<JsonResult> HallDetails(string hallNumber)
        {
            var hall = await _hallService.Get(hallNumber);
            return new JsonResult(hall);
        }
        [HttpPost]
        [Route("add")]
        public async Task Add(string hallNumber, int seatsInRow, int numberOfRows)
        {
            await _hallService.AddHallAsync(hallNumber, seatsInRow, numberOfRows);
        }
        [HttpGet]
        [Route("details/list")]
        public async Task<JsonResult> GetHallsList()
        {
            var halls = await _hallService.GetAllAsync();
            return new JsonResult(halls);
        }
        [HttpPost]
        [Route("remove/{hallNumber}")]
        public async Task Remove(string hallNumber)
        {
            await _hallService.Remove(hallNumber);
        }
    }
}